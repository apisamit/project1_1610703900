﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Manager;

public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] AudioClip playerFireSound;
        [SerializeField] float playerFireSoundVolume = 0.3f;
        [SerializeField] AudioClip playerDied;
        [SerializeField] float playerDiedSoundVolume = 0.3f;

    private Vector2 movementInput = Vector2.zero;

        public GameObject PlayerBullet;
        public GameObject bulletPosition;

        public float Health = 200;
        public static PlayerController sp; 
        private float Damage = 10;

    private void Start()
    {
        sp = this;
    }

    private void Update()
        {
            Move();
            Fire();
        }

    private void Move()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;

            var newXPos = transform.position.x + inPutVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + inPutVelocity.y * Time.deltaTime;

            transform.position = new Vector2(newXPos, newYPos);
    }

    private void Fire()
    {
            if (Input.GetKeyDown("space"))
            {
                GameObject bullet = (GameObject)Instantiate(PlayerBullet);
                bullet.transform.position = bulletPosition.transform.position;
                SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
            
        }
            else if(Input.GetButtonDown("Fire1"))
            {
                GameObject bullet = (GameObject)Instantiate(PlayerBullet);
                bullet.transform.position = bulletPosition.transform.position;
                SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
            movementInput = context.ReadValue<Vector2>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
            if ((collision.tag == "EnemyShipTag") || (collision.tag == "BossShipTag") || (collision.tag == "EnemyBulletTag"))
            {

                Health -= Damage;

                if (Health <= 0)
                {
                AudioSource.PlayClipAtPoint(playerDied, Camera.main.transform.position, playerDiedSoundVolume);
                Destroy(gameObject);
                SceneManager.LoadScene("GameOver");
            }

            }
    }

    }
