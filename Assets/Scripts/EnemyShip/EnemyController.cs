﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using TMPro;
using Manager;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float enemyShipSpeed = 1f;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] AudioClip enemyDied;
        [SerializeField] float enemyDiedSoundVolume = 0.3f;

        public float Health = 10;
        private float Damage = 10;
        public TextMeshProUGUI text;

        private void Start()
        {

        }
        void Update()
        {
            MoveToPlayer();
        }

        private void MoveToPlayer()
        {
            Vector2 position = transform.position;

            position = new Vector2(position.x, position.y - enemyShipSpeed * Time.deltaTime);
            transform.position = position;
            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

            if(transform.position.y < min.y)
            {
                Destroy(gameObject);
            }

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if((collision.tag =="PlayerShipTag") || (collision.tag == "PlayerBulletTag"))
            {
                text = GameObject.Find("Canvas/TextScore").GetComponent<TextMeshProUGUI>();
                GameManager.spaceScore ++;
                text.text = "Score : " + GameManager.spaceScore;


                Health -= Damage;

                if (Health <= 0)
                {
                    AudioSource.PlayClipAtPoint(enemyDied, Camera.main.transform.position, enemyDiedSoundVolume);
                    Destroy(gameObject);
                }

            }
        }



    }    
}

