﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthhBar : MonoBehaviour
{
    private Image BossHP;
    public float CurrentHealth;
    private float MaxHealth = 1000;

    private void Start()
    {
        BossHP = GetComponent<Image>();
    }

    private void Update()
    {
        CurrentHealth = BossShip.Health;
        BossHP.fillAmount = CurrentHealth / MaxHealth;

        if(MaxHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
